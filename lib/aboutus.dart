import 'package:flutter/material.dart';


class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
        backgroundColor: Colors.orange,
      ),
      body: Container(
          constraints: BoxConstraints.expand(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 40,),
                CircleAvatar(
                    radius: 100,
                    backgroundImage: AssetImage('assets/images/na.jpg'),
                ),
                SizedBox(height: 20,),
                Center(child: Text('6450110004\nนางสาวซารีณา ชูจันทร์',style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                ),
                SizedBox(height: 50,),
                CircleAvatar(
                  radius: 100,
                  backgroundImage: AssetImage('assets/images/tukta.jpg'),
                ),
                SizedBox(height: 20,),
                Center(child: Text('6450110008\nนางสาวเพ็ญศิริ อินทร์ประหยัด',style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                ),
                SizedBox(height: 50,),
                CircleAvatar(
                  radius: 100,
                  backgroundImage: AssetImage('assets/images/bow.jpg'),
                ),
                SizedBox(height: 20,),
                Center(child: Text('6450110014\nนางสาวสุนิตา เรืองนุ้ย',style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                ),
                SizedBox(height: 50,),
                CircleAvatar(
                  radius: 100,
                  backgroundImage: AssetImage('assets/images/min.jpg'),
                ),
                SizedBox(height: 20,),
                Center(child: Text('6450110017\nนางสาวกิตติมา ช้างขาว',style: TextStyle(fontSize: 20),textAlign: TextAlign.center,),
                ),
                SizedBox(height: 50,),
              ],
            ),
          )
      ),
    );
  }
}

